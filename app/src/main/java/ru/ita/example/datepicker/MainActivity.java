package ru.ita.example.datepicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.niwattep.materialslidedatepicker.SlideDatePickerDialog;
import com.niwattep.materialslidedatepicker.SlideDatePickerDialogCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
// на основе https://github.com/niwattep/material-slide-date-picker
// https://github.com/wdullaer/MaterialDateTimePicker - круче)

public class MainActivity extends AppCompatActivity implements SlideDatePickerDialogCallback {
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.textview);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar endDate = Calendar.getInstance();
                endDate.set(Calendar.YEAR, 2100);
//                SlideDatePickerDialog.Builder builder = new SlideDatePickerDialog.Builder();
//                builder.setEndDate(endDate)
//                        .setLocale(Locale.getDefault())
//                        .setConfirmText("ОК")
//                        .setCancelText("отмена");
//                SlideDatePickerDialog dialog = builder.build();
//                dialog.show(getSupportFragmentManager(), "dialog");
                new SlideDatePickerDialog.Builder()
                        .setEndDate(endDate)
                        .setLocale(Locale.getDefault())
                        .setConfirmText("ОК")
                        .setCancelText("отмена")
                        .build().show(getSupportFragmentManager(), "TAG");
            }
        });
    }

    @Override
    public void onPositiveClick(int day, int month, int year, Calendar calendar) {
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault());
        textView.setText(format.format(calendar.getTime()));
    }
}
